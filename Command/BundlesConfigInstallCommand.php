<?php

namespace IiMedias\StaffBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use IiMedias\AdminBundle\Model\MenuElementQuery;

class BundlesConfigInstallCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('bundles:staff:config')
            ->setDescription('Récupère la configuration pour l\'administration de iimedias')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        MenuElementQuery::addElement('admin', 'staff', 'Staff', 'users', 'iimedias_staff_admin_index', array());
    }

}
