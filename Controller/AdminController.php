<?php

namespace IiMedias\StaffBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use IiMedias\StaffBundle\Model\StaffGroup;
use IiMedias\StaffBundle\Model\StaffGroupQuery;
use IiMedias\StaffBundle\Form\Type\StaffGroupType;
use IiMedias\StaffBundle\Model\StaffElement;
use IiMedias\StaffBundle\Model\StaffElementQuery;
use IiMedias\StaffBundle\Form\Type\StaffElementType;
use \DateTime;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class AdminController extends Controller
{
    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/staff", name="iimedias_staff_admin_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $staffGroups = StaffGroupQuery::getAll();

        return $this->render('IiMediasStaffBundle:Admin:index.html.twig', array(
            'currentNav'  => 'staff',
            'staffGroups' => $staffGroups,
        ));
    }

    /**
     * Vue du groupe
     *
     * @access public
     * @since 1.0.0 31/10/2016 Création -- sebii
     * @param IiMedias\StaffBundle\Model\StaffGroup $staffGroup
     * @Route("/admin/{_locale}/staff/{groupId}", name="iimedias_staff_admin_group", requirements={"_locale"="\w{2}", "groupId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("staffGroup", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"groupId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function group(StaffGroup $staffGroup)
    {
        $staffElements = StaffElementQuery::getAllByStaffGroup($staffGroup);
        
        return $this->render(
            'IiMediasStaffBundle:Admin:group.html.twig',
            array(
                'currentNav'    => 'staff',
                'staffGroup'    => $staffGroup,
                'staffElements' => $staffElements,
            )
        );
    }

    /**
     * Vue de l'élément
     *
     * @access public
     * @since 1.0.0 10/11/2016 Création -- sebii
     * @param IiMedias\StaffBundle\Model\StaffGroup $staffGroup
     * @param IiMedias\StaffBundle\Model\StaffElement $staffElement
     * @Route("/admin/{_locale}/staff/{groupId}/{staffId}", name="iimedias_staff_admin_element", requirements={"_locale"="\w{2}", "groupId"="\d+", "staffId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("staffGroup", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"groupId": "id"}})
     * @ParamConverter("staffElement", class="IiMedias\StaffBundle\Model\StaffElement", options={"mapping"={"groupId": "staffGroupId", "staffId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function element(StaffGroup $staffGroup, StaffElement $staffElement)
    {
        return $this->render(
            'IiMediasStaffBundle:Admin:element.html.twig',
            array(
                'currentNav'   => 'staff',
                'staffGroup'   => $staffGroup,
                'staffElement' => $staffElement,
            )
        );
    }

    /**
     * Publication d'un élément
     *
     * @access public
     * @since 1.0.0 10/11/2016 Création -- sebii
     * @param IiMedias\StaffBundle\Model\StaffGroup $staffGroup
     * @param IiMedias\StaffBundle\Model\StaffElement $staffElement
     * @Route("/admin/{_locale}/staff/{groupId}/{staffId}/publish/{mode}", name="iimedias_staff_admin_elementpublish", requirements={"_locale"="\w{2}", "groupId"="\d+", "staffId"="\d+", "mode"="now|delay|none"}, defaults={"_locale"="fr"})
     * @ParamConverter("staffGroup", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"groupId": "id"}})
     * @ParamConverter("staffElement", class="IiMedias\StaffBundle\Model\StaffElement", options={"mapping"={"groupId": "staffGroupId", "staffId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function elementPublish(Request $request,StaffGroup $staffGroup, StaffElement $staffElement, $mode)
    {
        switch ($mode) {
            case 'now':
                $staffElement
                    ->setEnable(true)
                    ->setPublishedAt(new DateTime('now'))
                    ->save()
                ;
                break;
            case 'delay':
                if ($request->isMethod('post')) {
                    $staffElement
                        ->setEnable(true)
                        ->setPublishedAt(new DateTime($request->query->get('publishAt')))
                        ->save()
                    ;
                }
            break;
            case 'none':
            default:
                $staffElement
                    ->setEnable(false)
                    ->setPublishedAt(new DateTime('now +100year'))
                    ->save()
                ;
        }
        return $this->redirect(
            $this->generateUrl(
                'iimedias_staff_admin_element',
                array(
                    'groupId' => $staffGroup->getId(),
                    'staffId' => $staffElement->getId(),
                )
            )
        );
    }

    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request
     * @Route("/admin/{_locale}/staff/organize", name="iimedias_staff_admin_organize", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function organize(Request $request)
    {
        $staffGroups = StaffGroupQuery::getAll();

        if ($request->isMethod('post')) {
            $staffElements = StaffElementQuery::getAll();
            $organizeDatas = json_decode($request->request->get('organizeJson'), true);
            foreach ($staffGroups as $staffGroup) {
                foreach ($organizeDatas as $organizeData) {
                    if ($organizeData['type'] == 'group' && $organizeData['groupId'] == $staffGroup->getId()) {
                        $staffGroup
                            ->setPosition($organizeData['groupPosition'])
                            ->save()
                        ;
                    }
                }
            }
            foreach ($staffElements as $staffElement) {
                foreach ($organizeDatas as $organizeData) {
                    if ($organizeData['type'] == 'element' && $organizeData['elementId'] == $staffElement->getId()) {
                        $staffElement
                            ->setStaffGroupId($organizeData['elementGroupId'])
                            ->setPosition($organizeData['elementPosition'])
                            ->save()
                        ;
                    }
                }
            }
            return $this->redirect(
                $this->generateUrl('iimedias_staff_admin_index')
            );
        }

        return $this->render(
            'IiMediasStaffBundle:Admin:organize.html.twig',
            array(
                'currentNav'  => 'staff',
                'staffGroups' => $staffGroups,
            )
        );
    }

    /**
     * Formulaire d'un groupe de menu
     *
     * @access public
     * @since 1.0.0 28/10/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param string $mode
     * @Route("/admin/{_locale}/staff/create", name="iimedias_staff_admin_create", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "mode"="create"})
     * @Route("/admin/{_locale}/staff/{groupId}/edit", name="iimedias_staff_admin_edit", requirements={"_locale"="\w{2}", "groupId"="\d+"}, defaults={"_locale"="fr", "mode"="edit"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function formGroup(Request $request, $mode)
    {
        switch ($mode) {
            case 'edit':
                $staffGroup = StaffGroupQuery::getOneById($request->get('groupId'));
                break;
            case 'create':
            default:
            $staffGroup = new StaffGroup();
                break;
        }

        $form = $this->createForm(
            StaffGroupType::class,
            $staffGroup,
            array(
                'action' => $request->getUri(),
            )
        );

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($mode == 'create') {
                $staffGroup
                    ->setPosition(StaffGroupQuery::getNextPosition())
                ;
            }
            $staffGroup
                ->save()
            ;
            return $this->redirect(
                $this->generateUrl(
                    'iimedias_staff_admin_group',
                    array(
                        'groupId' => $staffGroup->getId(),
                    )
                )
            );
        }

        return $this->render(
            'IiMediasStaffBundle:Admin:formGroup.html.twig',
            array(
                'currentNav' => 'staff',
                'mode'       => $mode,
                'staffGroup' => $staffGroup,
                'form'       => $form->createView(),
            )
        );
    }

    /**
     * Formulaire d'un élément de menu
     *
     * @access public
     * @since 1.0.0 28/10/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param string $mode
     * @Route("/admin/{_locale}/staff/{groupId}/create", name="iimedias_staff_admin_elementcreate", requirements={"_locale"="\w{2}", "groupId"="\d+"}, defaults={"_locale"="fr", "mode"="create"})
     * @Route("/admin/{_locale}/staff/{groupId}/{staffId}/edit", name="iimedias_staff_admin_elementedit", requirements={"_locale"="\w{2}", "groupId"="\d+"}, defaults={"_locale"="fr", "mode"="edit"})
     * @ParamConverter("staffGroup", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"groupId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function formElement(Request $request, StaffGroup $staffGroup, $mode)
    {
        switch ($mode) {
            case 'edit':
                $staffElement = StaffElementQuery::getOneByIdAndStaffGroup($request->get('staffId'), $staffGroup);
                break;
            case 'create':
            default:
                $staffElement = new StaffElement();
                $staffElement
                    ->setStaffGroup($staffGroup)
                ;
                break;
        }

        $form = $this->createForm(
            StaffElementType::class,
            $staffElement,
            array(
                'action' => $request->getUri(),
                'method' => 'post',
            )
        );

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                if ($mode == 'create') {
                    $staffElement
                        ->setEnable(false)
                        ->setPublishedAt(new DateTime())
                        ->setPosition(StaffElementQuery::getNextPosition($staffGroup));
                }

                $staffElement->save();

                if (!is_null($staffElement->getCover())) {
                    $staffElement->getCover()->move(
                        $this->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . $request->getBasePath() . DIRECTORY_SEPARATOR . 'ii' . DIRECTORY_SEPARATOR . 'staff',
                        $staffElement->getId() . '.jpg'
                    );
                }

                return $this->redirect(
                    $this->generateUrl(
                        'iimedias_staff_admin_element',
                        array(
                            'groupId' => $staffGroup->getId(),
                            'staffId' => $staffElement->getId(),
                        )
                    )
                );
            }
        }

        return $this->render(
            'IiMediasStaffBundle:Admin:formElement.html.twig',
            array(
                'currentNav'   => 'staff',
                'mode'         => $mode,
                'staffGroup'   => $staffGroup,
                'staffElement' => $staffElement,
                'form'         => $form->createView(),
            )
        );
    }

    /**
     * Monter le groupe d'un cran
     *
     * @access public
     * @since 1.0.0 31/10/2016 Création -- sebii
     * @param IiMedias\AdminBundle\Model\MenuElement $menuElement
     * @Route("/admin/{_locale}/staff/{groupId}/up", name="iimedias_staff_admin_groupup", requirements={"_locale"="\w{2}", "groupId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("staffGroup", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"groupId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function groupUp(StaffGroup $staffGroup)
    {
        $otherStaffGroup = StaffGroupQuery::getOneByPosition($staffGroup->getPosition() - 1);
        if (!is_null($otherStaffGroup)) {
            $staffGroup->positionUp();
            $otherStaffGroup->positionDown();
        }

        return $this->redirect(
            $this->generateUrl(
                'iimedias_staff_admin_index'
            )
        );
    }

    /**
     * Monter le staff d'un cran
     *
     * @access public
     * @since 1.0.0 31/10/2016 Création -- sebii
     * @param IiMedias\StaffBundle\Model\StaffElement $staffGroup
     * @Route("/admin/{_locale}/staff/{groupId}/{staffId}/up", name="iimedias_staff_admin_staffup", requirements={"_locale"="\w{2}", "groupId"="\d+", "staffId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("staffGroup", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"groupId": "id"}})
     * @ParamConverter("staffElement", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"staffId": "id", "groupId": "staffGroupId"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function staffUp(StaffGroup $staffGroup, StaffElement $staffElement)
    {
        $otherStaffElement = StaffElementQuery::getOneByStaffGroupAndPosition($staffGroup, $staffElement->getPosition() - 1);
        if (!is_null($otherStaffElement)) {
            $staffElement->positionUp();
            $otherStaffElement->positionDown();
        }

        return $this->redirect(
            $this->generateUrl(
                'iimedias_staff_admin_group'
            )
        );
    }

    /**
     * Descendre le groupe d'un cran
     *
     * @access public
     * @since 1.0.0 31/10/2016 Création -- sebii
     * @param IiMedias\AdminBundle\Model\MenuElement $menuElement
     * @Route("/admin/{_locale}/staff/{groupId}/down", name="iimedias_staff_admin_groupdown", requirements={"_locale"="\w{2}", "groupId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("staffGroup", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"groupId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function groupDown(StaffGroup $staffGroup)
    {
        $otherStaffGroup = StaffGroupQuery::getOneByPosition($staffGroup->getPosition() + 1);
        if (!is_null($otherStaffGroup)) {
            $staffGroup->positionDown();
            $otherStaffGroup->positionUp();
        }

        return $this->redirect(
            $this->generateUrl(
                'iimedias_staff_admin_index'
            )
        );
    }

    /**
     * Descendre le staff d'un cran
     *
     * @access public
     * @since 1.0.0 31/10/2016 Création -- sebii
     * @param IiMedias\StaffBundle\Model\StaffElement $staffGroup
     * @Route("/admin/{_locale}/staff/{groupId}/{staffId}/down", name="iimedias_staff_admin_staffdown", requirements={"_locale"="\w{2}", "groupId"="\d+", "staffId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("staffGroup", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"groupId": "id"}})
     * @ParamConverter("staffElement", class="IiMedias\StaffBundle\Model\StaffGroup", options={"mapping"={"staffId": "id", "groupId": "staffGroupId"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function staffDown(StaffGroup $staffGroup, StaffElement $staffElement)
    {
        $otherStaffElement = StaffElementQuery::getOneByStaffGroupAndPosition($staffGroup, $staffElement->getPosition() + 1);
        if (!is_null($otherStaffElement)) {
            $staffElement->positionDown();
            $otherStaffElement->positionUp();
        }

        return $this->redirect(
            $this->generateUrl(
                'iimedias_staff_admin_group'
            )
        );
    }
}
