<?php

namespace IiMedias\StaffBundle\Model;

use IiMedias\StaffBundle\Model\Base\StaffElement as BaseStaffElement;

/**
 * Skeleton subclass for representing a row from the 'staff_element_staelm' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class StaffElement extends BaseStaffElement
{
    protected $cover;

    public function getCover()
    {
        return $this->cover;
    }

    public function setCover($cover)
    {
        $this->cover = $cover;
        return $this;
    }
}
