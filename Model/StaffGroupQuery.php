<?php

namespace IiMedias\StaffBundle\Model;

use IiMedias\StaffBundle\Model\Base\StaffGroupQuery as BaseStaffGroupQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'staff_group_stagrp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class StaffGroupQuery extends BaseStaffGroupQuery
{
    public static function getAll()
    {
        $staffGroups = self::create()
//            ->setQueryKey('StaffGroupGetAll')
            ->orderByPosition()
            ->find()
        ;

        return $staffGroups;
    }

    public static function getOneById($staffGroupId)
    {
        $staffGroup = self::create()
            ->filterById($staffGroupId)
            ->findOne()
        ;

        return $staffGroup;
    }

    public static function getNextPosition()
    {
        $staffGroup = self::create()
//            ->setQueryKey('StaffGroupGetNextPosition')
            ->orderByPosition(Criteria::DESC)
            ->findOne()
        ;

        $nextPosition = is_null($staffGroup) ? 1 : $staffGroup->getPosition() + 1;
        return $nextPosition;
    }

    public static function getOneByPosition($position)
    {
        $staffGroup = self::create()
//            ->setQueryKey('StaffGroupGetOneByPosition')
            ->filterbyPosition($position)
            ->findOne()
        ;

        return $staffGroup;
    }
}
