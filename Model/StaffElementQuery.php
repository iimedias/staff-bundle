<?php

namespace IiMedias\StaffBundle\Model;

use IiMedias\StaffBundle\Model\Base\StaffElementQuery as BaseStaffElementQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'staff_element_staelm' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class StaffElementQuery extends BaseStaffElementQuery
{
    public static function getOneByIdAndStaffGroup($elementId, $staffGroup)
    {
        $element = self::create()
            ->filterByStaffGroup($staffGroup)
            ->filterById($elementId)
            ->findOne();

        return $element;
    }

    public static function getAll()
    {
        $elements = self::create()
            ->find()
        ;

        return $elements;
    }

    public static function getAllByStaffGroup(StaffGroup $staffGroup)
    {
        $elements = self::create()
            ->filterByStaffGroup($staffGroup)
            ->orderByPosition()
            ->find();

        return $elements;
    }

    public static function getNextPosition(StaffGroup $staffGroup)
    {
        $staffElement = self::create()
//            ->setQueryKey('StaffGroupGetNextPosition')
            ->filterbyStaffGroup($staffGroup)
            ->orderByPosition(Criteria::DESC)
            ->findOne()
        ;

        $nextPosition = is_null($staffElement) ? 1 : $staffElement->getPosition() + 1;
        return $nextPosition;
    }
}
