<?php

namespace IiMedias\StaffBundle\Model\Map;

use IiMedias\StaffBundle\Model\StaffElement;
use IiMedias\StaffBundle\Model\StaffElementQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'staff_element_staelm' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class StaffElementTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StaffBundle.Model.Map.StaffElementTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'staff_element_staelm';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StaffBundle\\Model\\StaffElement';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StaffBundle.Model.StaffElement';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the staelm_id field
     */
    const COL_STAELM_ID = 'staff_element_staelm.staelm_id';

    /**
     * the column name for the staelm_stagrp_id field
     */
    const COL_STAELM_STAGRP_ID = 'staff_element_staelm.staelm_stagrp_id';

    /**
     * the column name for the staelm_name field
     */
    const COL_STAELM_NAME = 'staff_element_staelm.staelm_name';

    /**
     * the column name for the staelm_country field
     */
    const COL_STAELM_COUNTRY = 'staff_element_staelm.staelm_country';

    /**
     * the column name for the staelm_video_url field
     */
    const COL_STAELM_VIDEO_URL = 'staff_element_staelm.staelm_video_url';

    /**
     * the column name for the staelm_enable field
     */
    const COL_STAELM_ENABLE = 'staff_element_staelm.staelm_enable';

    /**
     * the column name for the staelm_position field
     */
    const COL_STAELM_POSITION = 'staff_element_staelm.staelm_position';

    /**
     * the column name for the staelm_published_at field
     */
    const COL_STAELM_PUBLISHED_AT = 'staff_element_staelm.staelm_published_at';

    /**
     * the column name for the staelm_created_at field
     */
    const COL_STAELM_CREATED_AT = 'staff_element_staelm.staelm_created_at';

    /**
     * the column name for the staelm_updated_at field
     */
    const COL_STAELM_UPDATED_AT = 'staff_element_staelm.staelm_updated_at';

    /**
     * the column name for the staelm_slug field
     */
    const COL_STAELM_SLUG = 'staff_element_staelm.staelm_slug';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StaffGroupId', 'Name', 'Country', 'VideoUrl', 'Enable', 'Position', 'PublishedAt', 'CreatedAt', 'UpdatedAt', 'StaelmSlug', ),
        self::TYPE_CAMELNAME     => array('id', 'staffGroupId', 'name', 'country', 'videoUrl', 'enable', 'position', 'publishedAt', 'createdAt', 'updatedAt', 'staelmSlug', ),
        self::TYPE_COLNAME       => array(StaffElementTableMap::COL_STAELM_ID, StaffElementTableMap::COL_STAELM_STAGRP_ID, StaffElementTableMap::COL_STAELM_NAME, StaffElementTableMap::COL_STAELM_COUNTRY, StaffElementTableMap::COL_STAELM_VIDEO_URL, StaffElementTableMap::COL_STAELM_ENABLE, StaffElementTableMap::COL_STAELM_POSITION, StaffElementTableMap::COL_STAELM_PUBLISHED_AT, StaffElementTableMap::COL_STAELM_CREATED_AT, StaffElementTableMap::COL_STAELM_UPDATED_AT, StaffElementTableMap::COL_STAELM_SLUG, ),
        self::TYPE_FIELDNAME     => array('staelm_id', 'staelm_stagrp_id', 'staelm_name', 'staelm_country', 'staelm_video_url', 'staelm_enable', 'staelm_position', 'staelm_published_at', 'staelm_created_at', 'staelm_updated_at', 'staelm_slug', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StaffGroupId' => 1, 'Name' => 2, 'Country' => 3, 'VideoUrl' => 4, 'Enable' => 5, 'Position' => 6, 'PublishedAt' => 7, 'CreatedAt' => 8, 'UpdatedAt' => 9, 'StaelmSlug' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'staffGroupId' => 1, 'name' => 2, 'country' => 3, 'videoUrl' => 4, 'enable' => 5, 'position' => 6, 'publishedAt' => 7, 'createdAt' => 8, 'updatedAt' => 9, 'staelmSlug' => 10, ),
        self::TYPE_COLNAME       => array(StaffElementTableMap::COL_STAELM_ID => 0, StaffElementTableMap::COL_STAELM_STAGRP_ID => 1, StaffElementTableMap::COL_STAELM_NAME => 2, StaffElementTableMap::COL_STAELM_COUNTRY => 3, StaffElementTableMap::COL_STAELM_VIDEO_URL => 4, StaffElementTableMap::COL_STAELM_ENABLE => 5, StaffElementTableMap::COL_STAELM_POSITION => 6, StaffElementTableMap::COL_STAELM_PUBLISHED_AT => 7, StaffElementTableMap::COL_STAELM_CREATED_AT => 8, StaffElementTableMap::COL_STAELM_UPDATED_AT => 9, StaffElementTableMap::COL_STAELM_SLUG => 10, ),
        self::TYPE_FIELDNAME     => array('staelm_id' => 0, 'staelm_stagrp_id' => 1, 'staelm_name' => 2, 'staelm_country' => 3, 'staelm_video_url' => 4, 'staelm_enable' => 5, 'staelm_position' => 6, 'staelm_published_at' => 7, 'staelm_created_at' => 8, 'staelm_updated_at' => 9, 'staelm_slug' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('staff_element_staelm');
        $this->setPhpName('StaffElement');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StaffBundle\\Model\\StaffElement');
        $this->setPackage('src.IiMedias.StaffBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('staelm_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('staelm_stagrp_id', 'StaffGroupId', 'INTEGER', 'staff_group_stagrp', 'stagrp_id', true, null, null);
        $this->addColumn('staelm_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('staelm_country', 'Country', 'ARRAY', false, null, null);
        $this->addColumn('staelm_video_url', 'VideoUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('staelm_enable', 'Enable', 'BOOLEAN', true, 1, true);
        $this->addColumn('staelm_position', 'Position', 'INTEGER', true, null, null);
        $this->addColumn('staelm_published_at', 'PublishedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('staelm_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('staelm_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('staelm_slug', 'StaelmSlug', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('StaffGroup', '\\IiMedias\\StaffBundle\\Model\\StaffGroup', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':staelm_stagrp_id',
    1 => ':stagrp_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'sluggable' => array('slug_column' => 'staelm_slug', 'slug_pattern' => '{Name}', 'replace_pattern' => '/[^\w\/]+/u', 'replacement' => '-', 'separator' => '/', 'permanent' => 'true', 'scope_column' => '', 'unique_constraint' => 'true', ),
            'timestampable' => array('create_column' => 'staelm_created_at', 'update_column' => 'staelm_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'query_cache' => array('backend' => 'apc', 'lifetime' => '3600', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? StaffElementTableMap::CLASS_DEFAULT : StaffElementTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (StaffElement object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = StaffElementTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = StaffElementTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + StaffElementTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = StaffElementTableMap::OM_CLASS;
            /** @var StaffElement $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            StaffElementTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = StaffElementTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = StaffElementTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var StaffElement $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                StaffElementTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_ID);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_STAGRP_ID);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_NAME);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_COUNTRY);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_VIDEO_URL);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_ENABLE);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_POSITION);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_PUBLISHED_AT);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_CREATED_AT);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_UPDATED_AT);
            $criteria->addSelectColumn(StaffElementTableMap::COL_STAELM_SLUG);
        } else {
            $criteria->addSelectColumn($alias . '.staelm_id');
            $criteria->addSelectColumn($alias . '.staelm_stagrp_id');
            $criteria->addSelectColumn($alias . '.staelm_name');
            $criteria->addSelectColumn($alias . '.staelm_country');
            $criteria->addSelectColumn($alias . '.staelm_video_url');
            $criteria->addSelectColumn($alias . '.staelm_enable');
            $criteria->addSelectColumn($alias . '.staelm_position');
            $criteria->addSelectColumn($alias . '.staelm_published_at');
            $criteria->addSelectColumn($alias . '.staelm_created_at');
            $criteria->addSelectColumn($alias . '.staelm_updated_at');
            $criteria->addSelectColumn($alias . '.staelm_slug');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(StaffElementTableMap::DATABASE_NAME)->getTable(StaffElementTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(StaffElementTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(StaffElementTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new StaffElementTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a StaffElement or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or StaffElement object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffElementTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StaffBundle\Model\StaffElement) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(StaffElementTableMap::DATABASE_NAME);
            $criteria->add(StaffElementTableMap::COL_STAELM_ID, (array) $values, Criteria::IN);
        }

        $query = StaffElementQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            StaffElementTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                StaffElementTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the staff_element_staelm table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return StaffElementQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a StaffElement or Criteria object.
     *
     * @param mixed               $criteria Criteria or StaffElement object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffElementTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from StaffElement object
        }

        if ($criteria->containsKey(StaffElementTableMap::COL_STAELM_ID) && $criteria->keyContainsValue(StaffElementTableMap::COL_STAELM_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.StaffElementTableMap::COL_STAELM_ID.')');
        }


        // Set the correct dbName
        $query = StaffElementQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // StaffElementTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
StaffElementTableMap::buildTableMap();
