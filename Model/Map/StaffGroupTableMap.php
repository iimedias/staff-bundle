<?php

namespace IiMedias\StaffBundle\Model\Map;

use IiMedias\StaffBundle\Model\StaffGroup;
use IiMedias\StaffBundle\Model\StaffGroupQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'staff_group_stagrp' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class StaffGroupTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StaffBundle.Model.Map.StaffGroupTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'staff_group_stagrp';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StaffBundle\\Model\\StaffGroup';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StaffBundle.Model.StaffGroup';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the stagrp_id field
     */
    const COL_STAGRP_ID = 'staff_group_stagrp.stagrp_id';

    /**
     * the column name for the stagrp_code field
     */
    const COL_STAGRP_CODE = 'staff_group_stagrp.stagrp_code';

    /**
     * the column name for the stagrp_message field
     */
    const COL_STAGRP_MESSAGE = 'staff_group_stagrp.stagrp_message';

    /**
     * the column name for the stagrp_position field
     */
    const COL_STAGRP_POSITION = 'staff_group_stagrp.stagrp_position';

    /**
     * the column name for the stagrp_created_at field
     */
    const COL_STAGRP_CREATED_AT = 'staff_group_stagrp.stagrp_created_at';

    /**
     * the column name for the stagrp_updated_at field
     */
    const COL_STAGRP_UPDATED_AT = 'staff_group_stagrp.stagrp_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Code', 'Message', 'Position', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'code', 'message', 'position', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(StaffGroupTableMap::COL_STAGRP_ID, StaffGroupTableMap::COL_STAGRP_CODE, StaffGroupTableMap::COL_STAGRP_MESSAGE, StaffGroupTableMap::COL_STAGRP_POSITION, StaffGroupTableMap::COL_STAGRP_CREATED_AT, StaffGroupTableMap::COL_STAGRP_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('stagrp_id', 'stagrp_code', 'stagrp_message', 'stagrp_position', 'stagrp_created_at', 'stagrp_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Code' => 1, 'Message' => 2, 'Position' => 3, 'CreatedAt' => 4, 'UpdatedAt' => 5, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'code' => 1, 'message' => 2, 'position' => 3, 'createdAt' => 4, 'updatedAt' => 5, ),
        self::TYPE_COLNAME       => array(StaffGroupTableMap::COL_STAGRP_ID => 0, StaffGroupTableMap::COL_STAGRP_CODE => 1, StaffGroupTableMap::COL_STAGRP_MESSAGE => 2, StaffGroupTableMap::COL_STAGRP_POSITION => 3, StaffGroupTableMap::COL_STAGRP_CREATED_AT => 4, StaffGroupTableMap::COL_STAGRP_UPDATED_AT => 5, ),
        self::TYPE_FIELDNAME     => array('stagrp_id' => 0, 'stagrp_code' => 1, 'stagrp_message' => 2, 'stagrp_position' => 3, 'stagrp_created_at' => 4, 'stagrp_updated_at' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('staff_group_stagrp');
        $this->setPhpName('StaffGroup');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StaffBundle\\Model\\StaffGroup');
        $this->setPackage('src.IiMedias.StaffBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stagrp_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('stagrp_code', 'Code', 'VARCHAR', true, 255, null);
        $this->addColumn('stagrp_message', 'Message', 'VARCHAR', true, 255, null);
        $this->addColumn('stagrp_position', 'Position', 'INTEGER', true, null, null);
        $this->addColumn('stagrp_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('stagrp_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('StaffElement', '\\IiMedias\\StaffBundle\\Model\\StaffElement', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':staelm_stagrp_id',
    1 => ':stagrp_id',
  ),
), 'CASCADE', 'CASCADE', 'StaffElements', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'stagrp_created_at', 'update_column' => 'stagrp_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'query_cache' => array('backend' => 'apc', 'lifetime' => '3600', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to staff_group_stagrp     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        StaffElementTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? StaffGroupTableMap::CLASS_DEFAULT : StaffGroupTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (StaffGroup object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = StaffGroupTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = StaffGroupTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + StaffGroupTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = StaffGroupTableMap::OM_CLASS;
            /** @var StaffGroup $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            StaffGroupTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = StaffGroupTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = StaffGroupTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var StaffGroup $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                StaffGroupTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(StaffGroupTableMap::COL_STAGRP_ID);
            $criteria->addSelectColumn(StaffGroupTableMap::COL_STAGRP_CODE);
            $criteria->addSelectColumn(StaffGroupTableMap::COL_STAGRP_MESSAGE);
            $criteria->addSelectColumn(StaffGroupTableMap::COL_STAGRP_POSITION);
            $criteria->addSelectColumn(StaffGroupTableMap::COL_STAGRP_CREATED_AT);
            $criteria->addSelectColumn(StaffGroupTableMap::COL_STAGRP_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.stagrp_id');
            $criteria->addSelectColumn($alias . '.stagrp_code');
            $criteria->addSelectColumn($alias . '.stagrp_message');
            $criteria->addSelectColumn($alias . '.stagrp_position');
            $criteria->addSelectColumn($alias . '.stagrp_created_at');
            $criteria->addSelectColumn($alias . '.stagrp_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(StaffGroupTableMap::DATABASE_NAME)->getTable(StaffGroupTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(StaffGroupTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(StaffGroupTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new StaffGroupTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a StaffGroup or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or StaffGroup object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffGroupTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StaffBundle\Model\StaffGroup) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(StaffGroupTableMap::DATABASE_NAME);
            $criteria->add(StaffGroupTableMap::COL_STAGRP_ID, (array) $values, Criteria::IN);
        }

        $query = StaffGroupQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            StaffGroupTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                StaffGroupTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the staff_group_stagrp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return StaffGroupQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a StaffGroup or Criteria object.
     *
     * @param mixed               $criteria Criteria or StaffGroup object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffGroupTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from StaffGroup object
        }

        if ($criteria->containsKey(StaffGroupTableMap::COL_STAGRP_ID) && $criteria->keyContainsValue(StaffGroupTableMap::COL_STAGRP_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.StaffGroupTableMap::COL_STAGRP_ID.')');
        }


        // Set the correct dbName
        $query = StaffGroupQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // StaffGroupTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
StaffGroupTableMap::buildTableMap();
