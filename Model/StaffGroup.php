<?php

namespace IiMedias\StaffBundle\Model;

use IiMedias\StaffBundle\Model\Base\StaffGroup as BaseStaffGroup;

/**
 * Skeleton subclass for representing a row from the 'staff_group_stagrp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class StaffGroup extends BaseStaffGroup
{
    public function positionUp()
    {
        $this
            ->setPosition($this->getPosition() - 1)
            ->save()
        ;

        return $this;
    }

    public function positionDown()
    {
        $this
            ->setPosition($this->getPosition() + 1)
            ->save()
        ;

        return $this;
    }

    public function getElements()
    {
        $elements = StaffElementQuery::getAllByStaffGroup($this);

        return $elements;
    }
}
