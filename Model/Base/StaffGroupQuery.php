<?php

namespace IiMedias\StaffBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StaffBundle\Model\StaffGroup as ChildStaffGroup;
use IiMedias\StaffBundle\Model\StaffGroupQuery as ChildStaffGroupQuery;
use IiMedias\StaffBundle\Model\Map\StaffGroupTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'staff_group_stagrp' table.
 *
 *
 *
 * @method     ChildStaffGroupQuery orderById($order = Criteria::ASC) Order by the stagrp_id column
 * @method     ChildStaffGroupQuery orderByCode($order = Criteria::ASC) Order by the stagrp_code column
 * @method     ChildStaffGroupQuery orderByMessage($order = Criteria::ASC) Order by the stagrp_message column
 * @method     ChildStaffGroupQuery orderByPosition($order = Criteria::ASC) Order by the stagrp_position column
 * @method     ChildStaffGroupQuery orderByCreatedAt($order = Criteria::ASC) Order by the stagrp_created_at column
 * @method     ChildStaffGroupQuery orderByUpdatedAt($order = Criteria::ASC) Order by the stagrp_updated_at column
 *
 * @method     ChildStaffGroupQuery groupById() Group by the stagrp_id column
 * @method     ChildStaffGroupQuery groupByCode() Group by the stagrp_code column
 * @method     ChildStaffGroupQuery groupByMessage() Group by the stagrp_message column
 * @method     ChildStaffGroupQuery groupByPosition() Group by the stagrp_position column
 * @method     ChildStaffGroupQuery groupByCreatedAt() Group by the stagrp_created_at column
 * @method     ChildStaffGroupQuery groupByUpdatedAt() Group by the stagrp_updated_at column
 *
 * @method     ChildStaffGroupQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStaffGroupQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStaffGroupQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStaffGroupQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildStaffGroupQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildStaffGroupQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildStaffGroupQuery leftJoinStaffElement($relationAlias = null) Adds a LEFT JOIN clause to the query using the StaffElement relation
 * @method     ChildStaffGroupQuery rightJoinStaffElement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StaffElement relation
 * @method     ChildStaffGroupQuery innerJoinStaffElement($relationAlias = null) Adds a INNER JOIN clause to the query using the StaffElement relation
 *
 * @method     ChildStaffGroupQuery joinWithStaffElement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the StaffElement relation
 *
 * @method     ChildStaffGroupQuery leftJoinWithStaffElement() Adds a LEFT JOIN clause and with to the query using the StaffElement relation
 * @method     ChildStaffGroupQuery rightJoinWithStaffElement() Adds a RIGHT JOIN clause and with to the query using the StaffElement relation
 * @method     ChildStaffGroupQuery innerJoinWithStaffElement() Adds a INNER JOIN clause and with to the query using the StaffElement relation
 *
 * @method     \IiMedias\StaffBundle\Model\StaffElementQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStaffGroup findOne(ConnectionInterface $con = null) Return the first ChildStaffGroup matching the query
 * @method     ChildStaffGroup findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStaffGroup matching the query, or a new ChildStaffGroup object populated from the query conditions when no match is found
 *
 * @method     ChildStaffGroup findOneById(int $stagrp_id) Return the first ChildStaffGroup filtered by the stagrp_id column
 * @method     ChildStaffGroup findOneByCode(string $stagrp_code) Return the first ChildStaffGroup filtered by the stagrp_code column
 * @method     ChildStaffGroup findOneByMessage(string $stagrp_message) Return the first ChildStaffGroup filtered by the stagrp_message column
 * @method     ChildStaffGroup findOneByPosition(int $stagrp_position) Return the first ChildStaffGroup filtered by the stagrp_position column
 * @method     ChildStaffGroup findOneByCreatedAt(string $stagrp_created_at) Return the first ChildStaffGroup filtered by the stagrp_created_at column
 * @method     ChildStaffGroup findOneByUpdatedAt(string $stagrp_updated_at) Return the first ChildStaffGroup filtered by the stagrp_updated_at column *

 * @method     ChildStaffGroup requirePk($key, ConnectionInterface $con = null) Return the ChildStaffGroup by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffGroup requireOne(ConnectionInterface $con = null) Return the first ChildStaffGroup matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStaffGroup requireOneById(int $stagrp_id) Return the first ChildStaffGroup filtered by the stagrp_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffGroup requireOneByCode(string $stagrp_code) Return the first ChildStaffGroup filtered by the stagrp_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffGroup requireOneByMessage(string $stagrp_message) Return the first ChildStaffGroup filtered by the stagrp_message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffGroup requireOneByPosition(int $stagrp_position) Return the first ChildStaffGroup filtered by the stagrp_position column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffGroup requireOneByCreatedAt(string $stagrp_created_at) Return the first ChildStaffGroup filtered by the stagrp_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffGroup requireOneByUpdatedAt(string $stagrp_updated_at) Return the first ChildStaffGroup filtered by the stagrp_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStaffGroup[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStaffGroup objects based on current ModelCriteria
 * @method     ChildStaffGroup[]|ObjectCollection findById(int $stagrp_id) Return ChildStaffGroup objects filtered by the stagrp_id column
 * @method     ChildStaffGroup[]|ObjectCollection findByCode(string $stagrp_code) Return ChildStaffGroup objects filtered by the stagrp_code column
 * @method     ChildStaffGroup[]|ObjectCollection findByMessage(string $stagrp_message) Return ChildStaffGroup objects filtered by the stagrp_message column
 * @method     ChildStaffGroup[]|ObjectCollection findByPosition(int $stagrp_position) Return ChildStaffGroup objects filtered by the stagrp_position column
 * @method     ChildStaffGroup[]|ObjectCollection findByCreatedAt(string $stagrp_created_at) Return ChildStaffGroup objects filtered by the stagrp_created_at column
 * @method     ChildStaffGroup[]|ObjectCollection findByUpdatedAt(string $stagrp_updated_at) Return ChildStaffGroup objects filtered by the stagrp_updated_at column
 * @method     ChildStaffGroup[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StaffGroupQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StaffBundle\Model\Base\StaffGroupQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StaffBundle\\Model\\StaffGroup', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStaffGroupQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStaffGroupQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStaffGroupQuery) {
            return $criteria;
        }
        $query = new ChildStaffGroupQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStaffGroup|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StaffGroupTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = StaffGroupTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStaffGroup A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stagrp_id, stagrp_code, stagrp_message, stagrp_position, stagrp_created_at, stagrp_updated_at FROM staff_group_stagrp WHERE stagrp_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStaffGroup $obj */
            $obj = new ChildStaffGroup();
            $obj->hydrate($row);
            StaffGroupTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStaffGroup|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stagrp_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stagrp_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stagrp_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stagrp_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stagrp_code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE stagrp_code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE stagrp_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the stagrp_message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE stagrp_message = 'fooValue'
     * $query->filterByMessage('%fooValue%'); // WHERE stagrp_message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the stagrp_position column
     *
     * Example usage:
     * <code>
     * $query->filterByPosition(1234); // WHERE stagrp_position = 1234
     * $query->filterByPosition(array(12, 34)); // WHERE stagrp_position IN (12, 34)
     * $query->filterByPosition(array('min' => 12)); // WHERE stagrp_position > 12
     * </code>
     *
     * @param     mixed $position The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function filterByPosition($position = null, $comparison = null)
    {
        if (is_array($position)) {
            $useMinMax = false;
            if (isset($position['min'])) {
                $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_POSITION, $position['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($position['max'])) {
                $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_POSITION, $position['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_POSITION, $position, $comparison);
    }

    /**
     * Filter the query on the stagrp_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE stagrp_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE stagrp_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE stagrp_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the stagrp_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE stagrp_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE stagrp_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE stagrp_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StaffBundle\Model\StaffElement object
     *
     * @param \IiMedias\StaffBundle\Model\StaffElement|ObjectCollection $staffElement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStaffGroupQuery The current query, for fluid interface
     */
    public function filterByStaffElement($staffElement, $comparison = null)
    {
        if ($staffElement instanceof \IiMedias\StaffBundle\Model\StaffElement) {
            return $this
                ->addUsingAlias(StaffGroupTableMap::COL_STAGRP_ID, $staffElement->getStaffGroupId(), $comparison);
        } elseif ($staffElement instanceof ObjectCollection) {
            return $this
                ->useStaffElementQuery()
                ->filterByPrimaryKeys($staffElement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStaffElement() only accepts arguments of type \IiMedias\StaffBundle\Model\StaffElement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StaffElement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function joinStaffElement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StaffElement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StaffElement');
        }

        return $this;
    }

    /**
     * Use the StaffElement relation StaffElement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StaffBundle\Model\StaffElementQuery A secondary query class using the current class as primary query
     */
    public function useStaffElementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStaffElement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StaffElement', '\IiMedias\StaffBundle\Model\StaffElementQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStaffGroup $staffGroup Object to remove from the list of results
     *
     * @return $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function prune($staffGroup = null)
    {
        if ($staffGroup) {
            $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_ID, $staffGroup->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the staff_group_stagrp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffGroupTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            StaffGroupTableMap::clearInstancePool();
            StaffGroupTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffGroupTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(StaffGroupTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            StaffGroupTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            StaffGroupTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(StaffGroupTableMap::COL_STAGRP_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(StaffGroupTableMap::COL_STAGRP_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(StaffGroupTableMap::COL_STAGRP_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(StaffGroupTableMap::COL_STAGRP_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildStaffGroupQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(StaffGroupTableMap::COL_STAGRP_CREATED_AT);
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 3600)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(StaffGroupTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(StaffGroupTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // StaffGroupQuery
