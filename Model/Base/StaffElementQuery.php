<?php

namespace IiMedias\StaffBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StaffBundle\Model\StaffElement as ChildStaffElement;
use IiMedias\StaffBundle\Model\StaffElementQuery as ChildStaffElementQuery;
use IiMedias\StaffBundle\Model\Map\StaffElementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'staff_element_staelm' table.
 *
 *
 *
 * @method     ChildStaffElementQuery orderById($order = Criteria::ASC) Order by the staelm_id column
 * @method     ChildStaffElementQuery orderByStaffGroupId($order = Criteria::ASC) Order by the staelm_stagrp_id column
 * @method     ChildStaffElementQuery orderByName($order = Criteria::ASC) Order by the staelm_name column
 * @method     ChildStaffElementQuery orderByCountry($order = Criteria::ASC) Order by the staelm_country column
 * @method     ChildStaffElementQuery orderByVideoUrl($order = Criteria::ASC) Order by the staelm_video_url column
 * @method     ChildStaffElementQuery orderByEnable($order = Criteria::ASC) Order by the staelm_enable column
 * @method     ChildStaffElementQuery orderByPosition($order = Criteria::ASC) Order by the staelm_position column
 * @method     ChildStaffElementQuery orderByPublishedAt($order = Criteria::ASC) Order by the staelm_published_at column
 * @method     ChildStaffElementQuery orderByCreatedAt($order = Criteria::ASC) Order by the staelm_created_at column
 * @method     ChildStaffElementQuery orderByUpdatedAt($order = Criteria::ASC) Order by the staelm_updated_at column
 * @method     ChildStaffElementQuery orderByStaelmSlug($order = Criteria::ASC) Order by the staelm_slug column
 *
 * @method     ChildStaffElementQuery groupById() Group by the staelm_id column
 * @method     ChildStaffElementQuery groupByStaffGroupId() Group by the staelm_stagrp_id column
 * @method     ChildStaffElementQuery groupByName() Group by the staelm_name column
 * @method     ChildStaffElementQuery groupByCountry() Group by the staelm_country column
 * @method     ChildStaffElementQuery groupByVideoUrl() Group by the staelm_video_url column
 * @method     ChildStaffElementQuery groupByEnable() Group by the staelm_enable column
 * @method     ChildStaffElementQuery groupByPosition() Group by the staelm_position column
 * @method     ChildStaffElementQuery groupByPublishedAt() Group by the staelm_published_at column
 * @method     ChildStaffElementQuery groupByCreatedAt() Group by the staelm_created_at column
 * @method     ChildStaffElementQuery groupByUpdatedAt() Group by the staelm_updated_at column
 * @method     ChildStaffElementQuery groupByStaelmSlug() Group by the staelm_slug column
 *
 * @method     ChildStaffElementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStaffElementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStaffElementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStaffElementQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildStaffElementQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildStaffElementQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildStaffElementQuery leftJoinStaffGroup($relationAlias = null) Adds a LEFT JOIN clause to the query using the StaffGroup relation
 * @method     ChildStaffElementQuery rightJoinStaffGroup($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StaffGroup relation
 * @method     ChildStaffElementQuery innerJoinStaffGroup($relationAlias = null) Adds a INNER JOIN clause to the query using the StaffGroup relation
 *
 * @method     ChildStaffElementQuery joinWithStaffGroup($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the StaffGroup relation
 *
 * @method     ChildStaffElementQuery leftJoinWithStaffGroup() Adds a LEFT JOIN clause and with to the query using the StaffGroup relation
 * @method     ChildStaffElementQuery rightJoinWithStaffGroup() Adds a RIGHT JOIN clause and with to the query using the StaffGroup relation
 * @method     ChildStaffElementQuery innerJoinWithStaffGroup() Adds a INNER JOIN clause and with to the query using the StaffGroup relation
 *
 * @method     \IiMedias\StaffBundle\Model\StaffGroupQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStaffElement findOne(ConnectionInterface $con = null) Return the first ChildStaffElement matching the query
 * @method     ChildStaffElement findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStaffElement matching the query, or a new ChildStaffElement object populated from the query conditions when no match is found
 *
 * @method     ChildStaffElement findOneById(int $staelm_id) Return the first ChildStaffElement filtered by the staelm_id column
 * @method     ChildStaffElement findOneByStaffGroupId(int $staelm_stagrp_id) Return the first ChildStaffElement filtered by the staelm_stagrp_id column
 * @method     ChildStaffElement findOneByName(string $staelm_name) Return the first ChildStaffElement filtered by the staelm_name column
 * @method     ChildStaffElement findOneByCountry(array $staelm_country) Return the first ChildStaffElement filtered by the staelm_country column
 * @method     ChildStaffElement findOneByVideoUrl(string $staelm_video_url) Return the first ChildStaffElement filtered by the staelm_video_url column
 * @method     ChildStaffElement findOneByEnable(boolean $staelm_enable) Return the first ChildStaffElement filtered by the staelm_enable column
 * @method     ChildStaffElement findOneByPosition(int $staelm_position) Return the first ChildStaffElement filtered by the staelm_position column
 * @method     ChildStaffElement findOneByPublishedAt(string $staelm_published_at) Return the first ChildStaffElement filtered by the staelm_published_at column
 * @method     ChildStaffElement findOneByCreatedAt(string $staelm_created_at) Return the first ChildStaffElement filtered by the staelm_created_at column
 * @method     ChildStaffElement findOneByUpdatedAt(string $staelm_updated_at) Return the first ChildStaffElement filtered by the staelm_updated_at column
 * @method     ChildStaffElement findOneByStaelmSlug(string $staelm_slug) Return the first ChildStaffElement filtered by the staelm_slug column *

 * @method     ChildStaffElement requirePk($key, ConnectionInterface $con = null) Return the ChildStaffElement by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOne(ConnectionInterface $con = null) Return the first ChildStaffElement matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStaffElement requireOneById(int $staelm_id) Return the first ChildStaffElement filtered by the staelm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByStaffGroupId(int $staelm_stagrp_id) Return the first ChildStaffElement filtered by the staelm_stagrp_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByName(string $staelm_name) Return the first ChildStaffElement filtered by the staelm_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByCountry(array $staelm_country) Return the first ChildStaffElement filtered by the staelm_country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByVideoUrl(string $staelm_video_url) Return the first ChildStaffElement filtered by the staelm_video_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByEnable(boolean $staelm_enable) Return the first ChildStaffElement filtered by the staelm_enable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByPosition(int $staelm_position) Return the first ChildStaffElement filtered by the staelm_position column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByPublishedAt(string $staelm_published_at) Return the first ChildStaffElement filtered by the staelm_published_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByCreatedAt(string $staelm_created_at) Return the first ChildStaffElement filtered by the staelm_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByUpdatedAt(string $staelm_updated_at) Return the first ChildStaffElement filtered by the staelm_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStaffElement requireOneByStaelmSlug(string $staelm_slug) Return the first ChildStaffElement filtered by the staelm_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStaffElement[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStaffElement objects based on current ModelCriteria
 * @method     ChildStaffElement[]|ObjectCollection findById(int $staelm_id) Return ChildStaffElement objects filtered by the staelm_id column
 * @method     ChildStaffElement[]|ObjectCollection findByStaffGroupId(int $staelm_stagrp_id) Return ChildStaffElement objects filtered by the staelm_stagrp_id column
 * @method     ChildStaffElement[]|ObjectCollection findByName(string $staelm_name) Return ChildStaffElement objects filtered by the staelm_name column
 * @method     ChildStaffElement[]|ObjectCollection findByCountry(array $staelm_country) Return ChildStaffElement objects filtered by the staelm_country column
 * @method     ChildStaffElement[]|ObjectCollection findByVideoUrl(string $staelm_video_url) Return ChildStaffElement objects filtered by the staelm_video_url column
 * @method     ChildStaffElement[]|ObjectCollection findByEnable(boolean $staelm_enable) Return ChildStaffElement objects filtered by the staelm_enable column
 * @method     ChildStaffElement[]|ObjectCollection findByPosition(int $staelm_position) Return ChildStaffElement objects filtered by the staelm_position column
 * @method     ChildStaffElement[]|ObjectCollection findByPublishedAt(string $staelm_published_at) Return ChildStaffElement objects filtered by the staelm_published_at column
 * @method     ChildStaffElement[]|ObjectCollection findByCreatedAt(string $staelm_created_at) Return ChildStaffElement objects filtered by the staelm_created_at column
 * @method     ChildStaffElement[]|ObjectCollection findByUpdatedAt(string $staelm_updated_at) Return ChildStaffElement objects filtered by the staelm_updated_at column
 * @method     ChildStaffElement[]|ObjectCollection findByStaelmSlug(string $staelm_slug) Return ChildStaffElement objects filtered by the staelm_slug column
 * @method     ChildStaffElement[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StaffElementQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StaffBundle\Model\Base\StaffElementQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StaffBundle\\Model\\StaffElement', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStaffElementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStaffElementQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStaffElementQuery) {
            return $criteria;
        }
        $query = new ChildStaffElementQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStaffElement|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StaffElementTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = StaffElementTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStaffElement A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT staelm_id, staelm_stagrp_id, staelm_name, staelm_country, staelm_video_url, staelm_enable, staelm_position, staelm_published_at, staelm_created_at, staelm_updated_at, staelm_slug FROM staff_element_staelm WHERE staelm_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStaffElement $obj */
            $obj = new ChildStaffElement();
            $obj->hydrate($row);
            StaffElementTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStaffElement|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the staelm_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE staelm_id = 1234
     * $query->filterById(array(12, 34)); // WHERE staelm_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE staelm_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_ID, $id, $comparison);
    }

    /**
     * Filter the query on the staelm_stagrp_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStaffGroupId(1234); // WHERE staelm_stagrp_id = 1234
     * $query->filterByStaffGroupId(array(12, 34)); // WHERE staelm_stagrp_id IN (12, 34)
     * $query->filterByStaffGroupId(array('min' => 12)); // WHERE staelm_stagrp_id > 12
     * </code>
     *
     * @see       filterByStaffGroup()
     *
     * @param     mixed $staffGroupId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByStaffGroupId($staffGroupId = null, $comparison = null)
    {
        if (is_array($staffGroupId)) {
            $useMinMax = false;
            if (isset($staffGroupId['min'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_STAGRP_ID, $staffGroupId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($staffGroupId['max'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_STAGRP_ID, $staffGroupId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_STAGRP_ID, $staffGroupId, $comparison);
    }

    /**
     * Filter the query on the staelm_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE staelm_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE staelm_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the staelm_country column
     *
     * @param     array $country The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        $key = $this->getAliasedColName(StaffElementTableMap::COL_STAELM_COUNTRY);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($country as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($country as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($country as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the staelm_video_url column
     *
     * Example usage:
     * <code>
     * $query->filterByVideoUrl('fooValue');   // WHERE staelm_video_url = 'fooValue'
     * $query->filterByVideoUrl('%fooValue%'); // WHERE staelm_video_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $videoUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByVideoUrl($videoUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($videoUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_VIDEO_URL, $videoUrl, $comparison);
    }

    /**
     * Filter the query on the staelm_enable column
     *
     * Example usage:
     * <code>
     * $query->filterByEnable(true); // WHERE staelm_enable = true
     * $query->filterByEnable('yes'); // WHERE staelm_enable = true
     * </code>
     *
     * @param     boolean|string $enable The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByEnable($enable = null, $comparison = null)
    {
        if (is_string($enable)) {
            $enable = in_array(strtolower($enable), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_ENABLE, $enable, $comparison);
    }

    /**
     * Filter the query on the staelm_position column
     *
     * Example usage:
     * <code>
     * $query->filterByPosition(1234); // WHERE staelm_position = 1234
     * $query->filterByPosition(array(12, 34)); // WHERE staelm_position IN (12, 34)
     * $query->filterByPosition(array('min' => 12)); // WHERE staelm_position > 12
     * </code>
     *
     * @param     mixed $position The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByPosition($position = null, $comparison = null)
    {
        if (is_array($position)) {
            $useMinMax = false;
            if (isset($position['min'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_POSITION, $position['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($position['max'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_POSITION, $position['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_POSITION, $position, $comparison);
    }

    /**
     * Filter the query on the staelm_published_at column
     *
     * Example usage:
     * <code>
     * $query->filterByPublishedAt('2011-03-14'); // WHERE staelm_published_at = '2011-03-14'
     * $query->filterByPublishedAt('now'); // WHERE staelm_published_at = '2011-03-14'
     * $query->filterByPublishedAt(array('max' => 'yesterday')); // WHERE staelm_published_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $publishedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByPublishedAt($publishedAt = null, $comparison = null)
    {
        if (is_array($publishedAt)) {
            $useMinMax = false;
            if (isset($publishedAt['min'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_PUBLISHED_AT, $publishedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($publishedAt['max'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_PUBLISHED_AT, $publishedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_PUBLISHED_AT, $publishedAt, $comparison);
    }

    /**
     * Filter the query on the staelm_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE staelm_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE staelm_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE staelm_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the staelm_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE staelm_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE staelm_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE staelm_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(StaffElementTableMap::COL_STAELM_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the staelm_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByStaelmSlug('fooValue');   // WHERE staelm_slug = 'fooValue'
     * $query->filterByStaelmSlug('%fooValue%'); // WHERE staelm_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $staelmSlug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByStaelmSlug($staelmSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($staelmSlug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_SLUG, $staelmSlug, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StaffBundle\Model\StaffGroup object
     *
     * @param \IiMedias\StaffBundle\Model\StaffGroup|ObjectCollection $staffGroup The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterByStaffGroup($staffGroup, $comparison = null)
    {
        if ($staffGroup instanceof \IiMedias\StaffBundle\Model\StaffGroup) {
            return $this
                ->addUsingAlias(StaffElementTableMap::COL_STAELM_STAGRP_ID, $staffGroup->getId(), $comparison);
        } elseif ($staffGroup instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StaffElementTableMap::COL_STAELM_STAGRP_ID, $staffGroup->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStaffGroup() only accepts arguments of type \IiMedias\StaffBundle\Model\StaffGroup or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StaffGroup relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function joinStaffGroup($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StaffGroup');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StaffGroup');
        }

        return $this;
    }

    /**
     * Use the StaffGroup relation StaffGroup object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StaffBundle\Model\StaffGroupQuery A secondary query class using the current class as primary query
     */
    public function useStaffGroupQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStaffGroup($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StaffGroup', '\IiMedias\StaffBundle\Model\StaffGroupQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStaffElement $staffElement Object to remove from the list of results
     *
     * @return $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function prune($staffElement = null)
    {
        if ($staffElement) {
            $this->addUsingAlias(StaffElementTableMap::COL_STAELM_ID, $staffElement->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the staff_element_staelm table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffElementTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            StaffElementTableMap::clearInstancePool();
            StaffElementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffElementTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(StaffElementTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            StaffElementTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            StaffElementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // sluggable behavior

    /**
     * Filter the query on the slug column
     *
     * @param     string $slug The value to use as filter.
     *
     * @return    $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function filterBySlug($slug)
    {
        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_SLUG, $slug, Criteria::EQUAL);
    }

    /**
     * Find one object based on its slug
     *
     * @param     string $slug The value to use as filter.
     * @param     ConnectionInterface $con The optional connection object
     *
     * @return    ChildStaffElement the result, formatted by the current formatter
     */
    public function findOneBySlug($slug, $con = null)
    {
        return $this->filterBySlug($slug)->findOne($con);
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(StaffElementTableMap::COL_STAELM_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(StaffElementTableMap::COL_STAELM_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(StaffElementTableMap::COL_STAELM_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(StaffElementTableMap::COL_STAELM_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildStaffElementQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(StaffElementTableMap::COL_STAELM_CREATED_AT);
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 3600)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(StaffElementTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(StaffElementTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // StaffElementQuery
