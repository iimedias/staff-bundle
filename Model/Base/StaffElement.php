<?php

namespace IiMedias\StaffBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\StaffBundle\Model\StaffElement as ChildStaffElement;
use IiMedias\StaffBundle\Model\StaffElementQuery as ChildStaffElementQuery;
use IiMedias\StaffBundle\Model\StaffGroup as ChildStaffGroup;
use IiMedias\StaffBundle\Model\StaffGroupQuery as ChildStaffGroupQuery;
use IiMedias\StaffBundle\Model\Map\StaffElementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'staff_element_staelm' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.StaffBundle.Model.Base
 */
abstract class StaffElement implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\StaffBundle\\Model\\Map\\StaffElementTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the staelm_id field.
     *
     * @var        int
     */
    protected $staelm_id;

    /**
     * The value for the staelm_stagrp_id field.
     *
     * @var        int
     */
    protected $staelm_stagrp_id;

    /**
     * The value for the staelm_name field.
     *
     * @var        string
     */
    protected $staelm_name;

    /**
     * The value for the staelm_country field.
     *
     * @var        array
     */
    protected $staelm_country;

    /**
     * The unserialized $staelm_country value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $staelm_country_unserialized;

    /**
     * The value for the staelm_video_url field.
     *
     * @var        string
     */
    protected $staelm_video_url;

    /**
     * The value for the staelm_enable field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $staelm_enable;

    /**
     * The value for the staelm_position field.
     *
     * @var        int
     */
    protected $staelm_position;

    /**
     * The value for the staelm_published_at field.
     *
     * @var        DateTime
     */
    protected $staelm_published_at;

    /**
     * The value for the staelm_created_at field.
     *
     * @var        DateTime
     */
    protected $staelm_created_at;

    /**
     * The value for the staelm_updated_at field.
     *
     * @var        DateTime
     */
    protected $staelm_updated_at;

    /**
     * The value for the staelm_slug field.
     *
     * @var        string
     */
    protected $staelm_slug;

    /**
     * @var        ChildStaffGroup
     */
    protected $aStaffGroup;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->staelm_enable = true;
    }

    /**
     * Initializes internal state of IiMedias\StaffBundle\Model\Base\StaffElement object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>StaffElement</code> instance.  If
     * <code>obj</code> is an instance of <code>StaffElement</code>, delegates to
     * <code>equals(StaffElement)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|StaffElement The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [staelm_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->staelm_id;
    }

    /**
     * Get the [staelm_stagrp_id] column value.
     *
     * @return int
     */
    public function getStaffGroupId()
    {
        return $this->staelm_stagrp_id;
    }

    /**
     * Get the [staelm_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->staelm_name;
    }

    /**
     * Get the [staelm_country] column value.
     *
     * @return array
     */
    public function getCountry()
    {
        if (null === $this->staelm_country_unserialized) {
            $this->staelm_country_unserialized = array();
        }
        if (!$this->staelm_country_unserialized && null !== $this->staelm_country) {
            $staelm_country_unserialized = substr($this->staelm_country, 2, -2);
            $this->staelm_country_unserialized = $staelm_country_unserialized ? explode(' | ', $staelm_country_unserialized) : array();
        }

        return $this->staelm_country_unserialized;
    }

    /**
     * Get the [staelm_video_url] column value.
     *
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->staelm_video_url;
    }

    /**
     * Get the [staelm_enable] column value.
     *
     * @return boolean
     */
    public function getEnable()
    {
        return $this->staelm_enable;
    }

    /**
     * Get the [staelm_enable] column value.
     *
     * @return boolean
     */
    public function isEnable()
    {
        return $this->getEnable();
    }

    /**
     * Get the [staelm_position] column value.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->staelm_position;
    }

    /**
     * Get the [optionally formatted] temporal [staelm_published_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPublishedAt($format = NULL)
    {
        if ($format === null) {
            return $this->staelm_published_at;
        } else {
            return $this->staelm_published_at instanceof \DateTimeInterface ? $this->staelm_published_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [staelm_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->staelm_created_at;
        } else {
            return $this->staelm_created_at instanceof \DateTimeInterface ? $this->staelm_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [staelm_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->staelm_updated_at;
        } else {
            return $this->staelm_updated_at instanceof \DateTimeInterface ? $this->staelm_updated_at->format($format) : null;
        }
    }

    /**
     * Get the [staelm_slug] column value.
     *
     * @return string
     */
    public function getStaelmSlug()
    {
        return $this->staelm_slug;
    }

    /**
     * Set the value of [staelm_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->staelm_id !== $v) {
            $this->staelm_id = $v;
            $this->modifiedColumns[StaffElementTableMap::COL_STAELM_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [staelm_stagrp_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setStaffGroupId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->staelm_stagrp_id !== $v) {
            $this->staelm_stagrp_id = $v;
            $this->modifiedColumns[StaffElementTableMap::COL_STAELM_STAGRP_ID] = true;
        }

        if ($this->aStaffGroup !== null && $this->aStaffGroup->getId() !== $v) {
            $this->aStaffGroup = null;
        }

        return $this;
    } // setStaffGroupId()

    /**
     * Set the value of [staelm_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->staelm_name !== $v) {
            $this->staelm_name = $v;
            $this->modifiedColumns[StaffElementTableMap::COL_STAELM_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [staelm_country] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setCountry($v)
    {
        if ($this->staelm_country_unserialized !== $v) {
            $this->staelm_country_unserialized = $v;
            $this->staelm_country = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[StaffElementTableMap::COL_STAELM_COUNTRY] = true;
        }

        return $this;
    } // setCountry()

    /**
     * Set the value of [staelm_video_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setVideoUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->staelm_video_url !== $v) {
            $this->staelm_video_url = $v;
            $this->modifiedColumns[StaffElementTableMap::COL_STAELM_VIDEO_URL] = true;
        }

        return $this;
    } // setVideoUrl()

    /**
     * Sets the value of the [staelm_enable] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setEnable($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->staelm_enable !== $v) {
            $this->staelm_enable = $v;
            $this->modifiedColumns[StaffElementTableMap::COL_STAELM_ENABLE] = true;
        }

        return $this;
    } // setEnable()

    /**
     * Set the value of [staelm_position] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setPosition($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->staelm_position !== $v) {
            $this->staelm_position = $v;
            $this->modifiedColumns[StaffElementTableMap::COL_STAELM_POSITION] = true;
        }

        return $this;
    } // setPosition()

    /**
     * Sets the value of [staelm_published_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setPublishedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->staelm_published_at !== null || $dt !== null) {
            if ($this->staelm_published_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->staelm_published_at->format("Y-m-d H:i:s.u")) {
                $this->staelm_published_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[StaffElementTableMap::COL_STAELM_PUBLISHED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setPublishedAt()

    /**
     * Sets the value of [staelm_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->staelm_created_at !== null || $dt !== null) {
            if ($this->staelm_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->staelm_created_at->format("Y-m-d H:i:s.u")) {
                $this->staelm_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[StaffElementTableMap::COL_STAELM_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [staelm_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->staelm_updated_at !== null || $dt !== null) {
            if ($this->staelm_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->staelm_updated_at->format("Y-m-d H:i:s.u")) {
                $this->staelm_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[StaffElementTableMap::COL_STAELM_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Set the value of [staelm_slug] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     */
    public function setStaelmSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->staelm_slug !== $v) {
            $this->staelm_slug = $v;
            $this->modifiedColumns[StaffElementTableMap::COL_STAELM_SLUG] = true;
        }

        return $this;
    } // setStaelmSlug()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->staelm_enable !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : StaffElementTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->staelm_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : StaffElementTableMap::translateFieldName('StaffGroupId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->staelm_stagrp_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : StaffElementTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->staelm_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : StaffElementTableMap::translateFieldName('Country', TableMap::TYPE_PHPNAME, $indexType)];
            $this->staelm_country = $col;
            $this->staelm_country_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : StaffElementTableMap::translateFieldName('VideoUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->staelm_video_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : StaffElementTableMap::translateFieldName('Enable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->staelm_enable = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : StaffElementTableMap::translateFieldName('Position', TableMap::TYPE_PHPNAME, $indexType)];
            $this->staelm_position = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : StaffElementTableMap::translateFieldName('PublishedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->staelm_published_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : StaffElementTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->staelm_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : StaffElementTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->staelm_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : StaffElementTableMap::translateFieldName('StaelmSlug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->staelm_slug = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = StaffElementTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\StaffBundle\\Model\\StaffElement'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aStaffGroup !== null && $this->staelm_stagrp_id !== $this->aStaffGroup->getId()) {
            $this->aStaffGroup = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StaffElementTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildStaffElementQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aStaffGroup = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see StaffElement::setDeleted()
     * @see StaffElement::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffElementTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildStaffElementQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(StaffElementTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            // sluggable behavior

            if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_SLUG) && $this->getStaelmSlug()) {
                $this->setStaelmSlug($this->makeSlugUnique($this->getStaelmSlug()));
            } elseif (!$this->getStaelmSlug()) {
                $this->setStaelmSlug($this->createSlug());
            }
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(StaffElementTableMap::COL_STAELM_CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(StaffElementTableMap::COL_STAELM_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(StaffElementTableMap::COL_STAELM_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                StaffElementTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStaffGroup !== null) {
                if ($this->aStaffGroup->isModified() || $this->aStaffGroup->isNew()) {
                    $affectedRows += $this->aStaffGroup->save($con);
                }
                $this->setStaffGroup($this->aStaffGroup);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[StaffElementTableMap::COL_STAELM_ID] = true;
        if (null !== $this->staelm_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . StaffElementTableMap::COL_STAELM_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_id';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_STAGRP_ID)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_stagrp_id';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_name';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_country';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_VIDEO_URL)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_video_url';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_ENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_enable';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_POSITION)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_position';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_PUBLISHED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_published_at';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_created_at';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_updated_at';
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'staelm_slug';
        }

        $sql = sprintf(
            'INSERT INTO staff_element_staelm (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'staelm_id':
                        $stmt->bindValue($identifier, $this->staelm_id, PDO::PARAM_INT);
                        break;
                    case 'staelm_stagrp_id':
                        $stmt->bindValue($identifier, $this->staelm_stagrp_id, PDO::PARAM_INT);
                        break;
                    case 'staelm_name':
                        $stmt->bindValue($identifier, $this->staelm_name, PDO::PARAM_STR);
                        break;
                    case 'staelm_country':
                        $stmt->bindValue($identifier, $this->staelm_country, PDO::PARAM_STR);
                        break;
                    case 'staelm_video_url':
                        $stmt->bindValue($identifier, $this->staelm_video_url, PDO::PARAM_STR);
                        break;
                    case 'staelm_enable':
                        $stmt->bindValue($identifier, (int) $this->staelm_enable, PDO::PARAM_INT);
                        break;
                    case 'staelm_position':
                        $stmt->bindValue($identifier, $this->staelm_position, PDO::PARAM_INT);
                        break;
                    case 'staelm_published_at':
                        $stmt->bindValue($identifier, $this->staelm_published_at ? $this->staelm_published_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'staelm_created_at':
                        $stmt->bindValue($identifier, $this->staelm_created_at ? $this->staelm_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'staelm_updated_at':
                        $stmt->bindValue($identifier, $this->staelm_updated_at ? $this->staelm_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'staelm_slug':
                        $stmt->bindValue($identifier, $this->staelm_slug, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = StaffElementTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getStaffGroupId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getCountry();
                break;
            case 4:
                return $this->getVideoUrl();
                break;
            case 5:
                return $this->getEnable();
                break;
            case 6:
                return $this->getPosition();
                break;
            case 7:
                return $this->getPublishedAt();
                break;
            case 8:
                return $this->getCreatedAt();
                break;
            case 9:
                return $this->getUpdatedAt();
                break;
            case 10:
                return $this->getStaelmSlug();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['StaffElement'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['StaffElement'][$this->hashCode()] = true;
        $keys = StaffElementTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getStaffGroupId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getCountry(),
            $keys[4] => $this->getVideoUrl(),
            $keys[5] => $this->getEnable(),
            $keys[6] => $this->getPosition(),
            $keys[7] => $this->getPublishedAt(),
            $keys[8] => $this->getCreatedAt(),
            $keys[9] => $this->getUpdatedAt(),
            $keys[10] => $this->getStaelmSlug(),
        );
        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTime) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTime) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aStaffGroup) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'staffGroup';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'staff_group_stagrp';
                        break;
                    default:
                        $key = 'StaffGroup';
                }

                $result[$key] = $this->aStaffGroup->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = StaffElementTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setStaffGroupId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setCountry($value);
                break;
            case 4:
                $this->setVideoUrl($value);
                break;
            case 5:
                $this->setEnable($value);
                break;
            case 6:
                $this->setPosition($value);
                break;
            case 7:
                $this->setPublishedAt($value);
                break;
            case 8:
                $this->setCreatedAt($value);
                break;
            case 9:
                $this->setUpdatedAt($value);
                break;
            case 10:
                $this->setStaelmSlug($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = StaffElementTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setStaffGroupId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCountry($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setVideoUrl($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setEnable($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPosition($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setPublishedAt($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setCreatedAt($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setUpdatedAt($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setStaelmSlug($arr[$keys[10]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(StaffElementTableMap::DATABASE_NAME);

        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_ID)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_ID, $this->staelm_id);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_STAGRP_ID)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_STAGRP_ID, $this->staelm_stagrp_id);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_NAME)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_NAME, $this->staelm_name);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_COUNTRY)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_COUNTRY, $this->staelm_country);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_VIDEO_URL)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_VIDEO_URL, $this->staelm_video_url);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_ENABLE)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_ENABLE, $this->staelm_enable);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_POSITION)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_POSITION, $this->staelm_position);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_PUBLISHED_AT)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_PUBLISHED_AT, $this->staelm_published_at);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_CREATED_AT)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_CREATED_AT, $this->staelm_created_at);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_UPDATED_AT)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_UPDATED_AT, $this->staelm_updated_at);
        }
        if ($this->isColumnModified(StaffElementTableMap::COL_STAELM_SLUG)) {
            $criteria->add(StaffElementTableMap::COL_STAELM_SLUG, $this->staelm_slug);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildStaffElementQuery::create();
        $criteria->add(StaffElementTableMap::COL_STAELM_ID, $this->staelm_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (staelm_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\StaffBundle\Model\StaffElement (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setStaffGroupId($this->getStaffGroupId());
        $copyObj->setName($this->getName());
        $copyObj->setCountry($this->getCountry());
        $copyObj->setVideoUrl($this->getVideoUrl());
        $copyObj->setEnable($this->getEnable());
        $copyObj->setPosition($this->getPosition());
        $copyObj->setPublishedAt($this->getPublishedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setStaelmSlug($this->getStaelmSlug());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\StaffBundle\Model\StaffElement Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildStaffGroup object.
     *
     * @param  ChildStaffGroup $v
     * @return $this|\IiMedias\StaffBundle\Model\StaffElement The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStaffGroup(ChildStaffGroup $v = null)
    {
        if ($v === null) {
            $this->setStaffGroupId(NULL);
        } else {
            $this->setStaffGroupId($v->getId());
        }

        $this->aStaffGroup = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStaffGroup object, it will not be re-added.
        if ($v !== null) {
            $v->addStaffElement($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStaffGroup object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStaffGroup The associated ChildStaffGroup object.
     * @throws PropelException
     */
    public function getStaffGroup(ConnectionInterface $con = null)
    {
        if ($this->aStaffGroup === null && ($this->staelm_stagrp_id !== null)) {
            $this->aStaffGroup = ChildStaffGroupQuery::create()->findPk($this->staelm_stagrp_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStaffGroup->addStaffElements($this);
             */
        }

        return $this->aStaffGroup;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aStaffGroup) {
            $this->aStaffGroup->removeStaffElement($this);
        }
        $this->staelm_id = null;
        $this->staelm_stagrp_id = null;
        $this->staelm_name = null;
        $this->staelm_country = null;
        $this->staelm_country_unserialized = null;
        $this->staelm_video_url = null;
        $this->staelm_enable = null;
        $this->staelm_position = null;
        $this->staelm_published_at = null;
        $this->staelm_created_at = null;
        $this->staelm_updated_at = null;
        $this->staelm_slug = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aStaffGroup = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(StaffElementTableMap::DEFAULT_STRING_FORMAT);
    }

    // sluggable behavior

    /**
     * Wrap the setter for slug value
     *
     * @param   string
     * @return  $this|StaffElement
     */
    public function setSlug($v)
    {
        return $this->setStaelmSlug($v);
    }

    /**
     * Wrap the getter for slug value
     *
     * @return  string
     */
    public function getSlug()
    {
        return $this->getStaelmSlug();
    }

    /**
     * Create a unique slug based on the object
     *
     * @return string The object slug
     */
    protected function createSlug()
    {
        $slug = $this->createRawSlug();
        $slug = $this->limitSlugSize($slug);
        $slug = $this->makeSlugUnique($slug);

        return $slug;
    }

    /**
     * Create the slug from the appropriate columns
     *
     * @return string
     */
    protected function createRawSlug()
    {
        return '' . $this->cleanupSlugPart($this->getName()) . '';
    }

    /**
     * Cleanup a string to make a slug of it
     * Removes special characters, replaces blanks with a separator, and trim it
     *
     * @param     string $slug        the text to slugify
     * @param     string $replacement the separator used by slug
     * @return    string               the slugified text
     */
    protected static function cleanupSlugPart($slug, $replacement = '-')
    {
        // transliterate
        if (function_exists('iconv')) {
            $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
        }

        // lowercase
        if (function_exists('mb_strtolower')) {
            $slug = mb_strtolower($slug);
        } else {
            $slug = strtolower($slug);
        }

        // remove accents resulting from OSX's iconv
        $slug = str_replace(array('\'', '`', '^'), '', $slug);

        // replace non letter or digits with separator
        $slug = preg_replace('/[^\w\/]+/u', $replacement, $slug);

        // trim
        $slug = trim($slug, $replacement);

        if (empty($slug)) {
            return 'n-a';
        }

        return $slug;
    }


    /**
     * Make sure the slug is short enough to accommodate the column size
     *
     * @param    string $slug            the slug to check
     *
     * @return string                        the truncated slug
     */
    protected static function limitSlugSize($slug, $incrementReservedSpace = 3)
    {
        // check length, as suffix could put it over maximum
        if (strlen($slug) > (255 - $incrementReservedSpace)) {
            $slug = substr($slug, 0, 255 - $incrementReservedSpace);
        }

        return $slug;
    }


    /**
     * Get the slug, ensuring its uniqueness
     *
     * @param    string $slug            the slug to check
     * @param    string $separator       the separator used by slug
     * @param    int    $alreadyExists   false for the first try, true for the second, and take the high count + 1
     * @return   string                   the unique slug
     */
    protected function makeSlugUnique($slug, $separator = '/', $alreadyExists = false)
    {
        if (!$alreadyExists) {
            $slug2 = $slug;
        } else {
            $slug2 = $slug . $separator;
        }

        $adapter = \Propel\Runtime\Propel::getServiceContainer()->getAdapter('default');
        $col = 'q.StaelmSlug';
        $compare = $alreadyExists ? $adapter->compareRegex($col, '?') : sprintf('%s = ?', $col);

        $query = \IiMedias\StaffBundle\Model\StaffElementQuery::create('q')
            ->where($compare, $alreadyExists ? '^' . $slug2 . '[0-9]+$' : $slug2)
            ->prune($this)
        ;

        if (!$alreadyExists) {
            $count = $query->count();
            if ($count > 0) {
                return $this->makeSlugUnique($slug, $separator, true);
            }

            return $slug2;
        }

        $adapter = \Propel\Runtime\Propel::getServiceContainer()->getAdapter('default');
        // Already exists
        $object = $query
            ->addDescendingOrderByColumn($adapter->strLength('staelm_slug'))
            ->addDescendingOrderByColumn('staelm_slug')
        ->findOne();

        // First duplicate slug
        if (null == $object) {
            return $slug2 . '1';
        }

        $slugNum = substr($object->getStaelmSlug(), strlen($slug) + 1);
        if (0 == $slugNum[0]) {
            $slugNum[0] = 1;
        }

        return $slug2 . ($slugNum + 1);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildStaffElement The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[StaffElementTableMap::COL_STAELM_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
