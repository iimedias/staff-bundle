<?php

namespace IiMedias\StaffBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class StaffGroupType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'code',
                TextType::class,
                array(
                    'required'   => true,
                    'label'      => 'Code',
                    'label_attr' => array(
                        'class' => 'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'attr'       => array(
                        'placeholder' => 'Code',
                        'class'       => 'form-control',
                    ),
                )
            )
            ->add(
                'message',
                TextType::class,
                array(
                    'required'   => true,
                    'label'      => 'Message',
                    'label_attr' => array(
                        'class' => 'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'attr'       => array(
                        'placeholder' => 'Message',
                        'class'       => 'form-control',
                    ),
                )
            )
            ->add(
                'submit',
                SubmitType::class,
                array(
                    'label' => 'Enregistrer',
                    'attr'  => array(
                        'placeholder' => 'Enregistrer',
                        'class'       => 'btn btn-primary',
                    ),
                )
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'IiMedias\StaffBundle\Model\StaffGroup',
                'name'       => 'staffGroup',
        ));
    }
}
