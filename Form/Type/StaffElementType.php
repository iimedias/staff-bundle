<?php

namespace IiMedias\StaffBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class StaffElementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $flagsChoicesList = array('br', 'cl', 'co', 'cu', 'do', 'ec', 'es', 'fr', 'gb', 'it', 'lv', 'mx', 'pt', 'ro', 'us');
        $flagsChoices     = array();
        foreach ($flagsChoicesList as $flagChoice) {
            $flagsChoices['admin.flags.choice.' . $flagChoice] = $flagChoice;
        }

        $builder
            ->add('name', TextType::class, array('required' => true))
            ->add('country', ChoiceType::class, array(
                    'choice_translation_domain' => 'messages',
                    'choices'                   => $flagsChoices,
                    'multiple'                  => true,
                    'required'                  => true,
            ))
            ->add('cover', FileType::class, array('required' => false))
            ->add('videoUrl', UrlType::class, array('required' => true))
            ->add('submit', SubmitType::class, array());
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'IiMedias\StaffBundle\Model\StaffElement',
                'name'       => 'staffElement',
        ));
    }
}
